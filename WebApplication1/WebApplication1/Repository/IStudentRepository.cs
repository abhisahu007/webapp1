﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Models;

namespace WebApplication1.Repository
{
   public interface IStudentRepository
    {
        public List<Student> GetStudents();

        void AddStudent(Student studentData);
    }
}
