﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApplication1.Models;
using WebApplication1.Repository;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {

        private readonly ILogger<HomeController> _logger;
        private readonly IStudentRepository _studentRepository;

        public HomeController(ILogger<HomeController> logger, IStudentRepository studentRepository)
        {
            _logger = logger;
            _studentRepository = studentRepository;
        }

        public IActionResult Index()
        {
            var data = _studentRepository.GetStudents();
            return View(data);
        }

        /*
        public ActionResult StudentDetail(Student student)
        {
            var data = _studentRepository.GetStudents();
            return View(data);
        } */

       
       /* public IActionResult AddStudent(Student studentData)
        {
            _studentRepository.AddStudent(studentData);
            return RedirectToAction("Index");
        } */

        public IActionResult Privacy(Student studentData)
        {
            _studentRepository.AddStudent(studentData);
            // return RedirectToAction("Index");
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
