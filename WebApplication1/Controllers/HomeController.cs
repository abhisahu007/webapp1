﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            List<Student> st = new List<Student>
            {
                new Student{ 
                    id=1,
                    firstName="Amit",
                    middleName="Kumar",
                    lastName= "Gupta",
                    email= "amit12@gmail.com",
                    mobile = "983512222",
                    state= "Uttar Pradesh",
                    city="Noida",
                    gender="Male",
                    address="JNU"
                },
                new Student{
                    id=2,
                    firstName="Ankit",
                    middleName="Kumar",
                    lastName= "Singh",
                    email= "ankit2@gmail.com",
                    mobile = "782221112",
                    state= "Bihar",
                    city="Patna",
                    gender="Male",
                    address="Station road Patna- 225120" 
                },
                new Student{
                    id=3,
                    firstName="Sunil",
                    middleName="Kumar",
                    lastName= "Verma",
                    email= "verma.sunil@gmail.com",
                    mobile = "915125222",
                    state= "Uttar Pradesh",
                    city="Lucknow",
                    gender="Male",
                    address="Charbagh station Lucknow" 
                },
                new Student{
                    id=4,
                    firstName="Harshit",
                    middleName="Ranjam",
                    lastName= "Srivastava",
                    email= "sriHR@gmail.com",
                    mobile = "7272585891",
                    state= "Uttar Pradesh",
                    city="Varanasi",
                    gender="Male",
                    address="Infront of cantoment, station road Varanasi"
                }

            };
            return View(st);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
