﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class Student
    {
        public int id { get; set; }

        public string firstName { get; set; }
        public string middleName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
        public string mobile { get; set; }
        public string state { get; set; }
        public string city { get; set; }
        public string gender { get; set; }
        public string higherEducation { get; set; }
        public string address { get; set; }

    }
}
